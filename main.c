#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define REVERSE 0
#define FORWARD 1

typedef struct node{
    int id;
    char msg[256];
    struct node* prev;
    struct node* next;
}node;

node* createNode(void);
int countNodes(node* pnode);
void insertData(node* pnode);
void printList(node* pnode);
void freeList(node* pnode);
void printNode(node* pnode);
int searchNodeID(node* pnode, int pid)

node* head = NULL;
node* tail = NULL;

node* createNode(void){
    node* newNode = NULL, *currNode;
    
    if((newNode = malloc(sizeof(node))) == NULL)
        return NULL;
    
    if(head){
        currNode = head;
        while(currNode->next)
            currNode = currNode->next;
        tail = currNode;
        tail->next = newNode;
        newNode->prev = tail;
        newNode->next = NULL;
        newNode->id = tail->id + 1;
    }else{
        head = newNode;
        head->prev = NULL;
        head->next = NULL;
        head->id = 1;
    }
    
    return newNode;
}

int countNodes(node* pnode){
    int count = 0;
    
    while(pnode){
        pnode = pnode->next;
        count++;
    }
    return count;
}

void insertData(node* pnode){
    printf("Type the message:");
    fgets(pnode->msg, 256, stdin);
    if(pnode->msg[strlen(pnode->msg) - 1] == '\n')
        pnode->msg[strlen(pnode->msg)] = '\0';
}

void printList(node* pnode){
    while(pnode){
        printNode(pnode);
        pnode = pnode->next;
    }
}

void freeList(node* pnode){
    node* tmpNode;
    
    while(pnode){
        tmpNode = pnode;
        pnode = pnode->next;
        free(pnode);
    }
}

void printNode(node* pnode){
        printf("Node ID: %d\n", pnode->id);
        printf("Node Message: %s", pnode->msg);
}

int searchNodeID(node* pnode, int pid){
    int len = 0;
    len = countNodes(head);
    
    while(pnode){
        if(pnode->id == pid)
            printNode(pnode);
        pnode = pnode->next;
    }
}

int main(int* argc, char** argv){
    node* newNode;
    int id = 1;
    
    head = createNode();
    insertData(head);
    
    newNode = createNode();
    insertData(newNode);
    
    newNode = createNode();
    insertData(newNode);
    
    printf("Number of nodes: %d\n", countNodes(head));
    printList(head);
    printf("Enter ID to search and print: \n");
    scanf(" %d", &id);
    searchNodeID(head, id);
    freeList(head);
    
    return 0;
}
